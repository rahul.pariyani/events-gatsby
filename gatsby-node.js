// /**
//  * Implement Gatsby's Node APIs in this file.
//  *
//  * See: https://www.gatsbyjs.com/docs/node-apis/
//  */

// // You can delete this file if you're not using it
const path = require(`path`)

module.exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const eventTemplate = path.resolve(`src/templates/event.js`)
  const res = await graphql(`
    query {
      allStrapiEvent {
        edges {
          node {
            Slug
          }
        }
      }
    }
  `)

  res.data.allStrapiEvent.edges.forEach(edge => {
    createPage({
      component: eventTemplate,
      path: `/blog/${edge.node.Slug}`,
      context: { Slug: edge.node.Slug },
    })
  })
}
