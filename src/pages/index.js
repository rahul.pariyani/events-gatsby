import * as React from "react"

import { graphql, useStaticQuery, Link } from "gatsby"

const IndexPage = () => {
  const data = useStaticQuery(graphql`
    query {
      allStrapiEvent {
        edges {
          node {
            Title
            Date
            Slug
          }
        }
      }
    }
  `)

  return (
    <div>
      <h1>Events Lists</h1>

      {data.allStrapiEvent.edges.map(event => {
        return (
          <Link to={`/blog/${event.node.Slug}`}>
            <div>
              <h4>{event.node.Title}</h4>
            </div>
          </Link>
        )
      })}
    </div>
  )
}

export default IndexPage
