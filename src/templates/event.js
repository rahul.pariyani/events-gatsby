import React from "react"
import { graphql } from "gatsby"

export const query = graphql`
  query($Slug: String!) {
    strapiEvent(Slug: { eq: $Slug }) {
      Title
      Date
      published_at(formatString: "DD MM YYYY")
      Body
      photos {
        url
      }
    }
  }
`

const Event = props => {
  return (
    <div>
      <h1>{props.data.strapiEvent.Title}</h1>
      <p> published at {props.data.strapiEvent.Date}</p>
      <div
        dangerouslySetInnerHTML={{ __html: props.data.strapiEvent.Body }}
      ></div>
    </div>
  )
}
export default Event
